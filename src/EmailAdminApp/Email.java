package EmailAdminApp;

import java.util.Random;
import java.util.Scanner;

public class Email {

    Scanner userInput = new Scanner(System.in);

    // setting up variables
    // defined as 'private' so that these cannot be accessed directly

    /*
    We need private variables for the employee's first name, last name, department,
    email, password, mail capacity (which is defaulted to 500), and an alter email
     */
    private String firstName;
    private String lastName;
    private String department;
    private String email;
    private String password;
    private int mailCapacity = 500;
    private String alter_email;


    // constructor to receive the first and last name
    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;


        // display the new employee's first name and last name
        System.out.println("NEW EMPLOYEE: " + this.firstName + " " + this.lastName);



        // call a method asking for the department - return the department
        this.department = this.setDept();


        // call a method that returns a random password
        this.password = this.generate_password(8);


        // combine elements to generate an email
        this.email = this.generateEmail();
    }

    // generate the email according to the given syntax
    // firstname.lastname@department.company.com
    private String generateEmail() {
        return this.firstName.toLowerCase() + "." + this.lastName.toLowerCase() + "@" + this.department.toLowerCase()
                + ".company.com";
    }

    // Ask for the department
    private String setDept() {
        System.out.println(
                "DEPARTMENT CODES\n1 for Sales\n2 for Development\n3 for Medical\n0 for None");
        boolean flag = false;
        do {
            System.out.println("Enter Department Code: ");
            int choice =  userInput.nextInt();
            switch (choice) {
                case 1:
                    return "Sales";
                case 2:
                    return "Development";
                case 3:
                    return "Medical";
                case 0:
                    return "None";
                default:
                    System.out.println("*** INVALID SELECTION ***");
            }
        } while (!flag);


        return null;
    }

    // Generating a random password
    private String generate_password(int length) {

        Random random = new Random();

        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String symbols = "!@#$%&?";
        String values = Capital_chars + Small_chars + numbers + symbols;

        String password = "";
        for (int i = 0; i < length; i++) {
            password = password + values.charAt(random.nextInt(values.length()));
        }
        return password;
    }

    // Change the password
    public void set_password() {
        boolean flag = false;
        do {
            System.out.print("ARE YOU SURE YOU WANT TO CHANGE YOUR PASSWORD? (Y/N) : ");
            char choice = userInput.next().charAt(0);
            // validation
            if (choice == 'Y' || choice == 'y') {

                System.out.println("Enter current password: ");
                String tempPassword = userInput.next();
                if (tempPassword.equals(this.password)) {
                    System.out.println("Enter new password: ");
                    this.password = userInput.next();
                    System.out.println(" *** PASSWORD UPDATED ***");
                    // unnecessary - just for my validation
                    System.out.println("NEW PASSWORD : " + this.password);
                    break;
                }
                else {
                    // "if tempPassword is not equal to this.password..."
                    System.out.println(" *** PASSWORD INCORRECT ***");
                }
            }
            else if (choice == 'N' || choice == 'n') {
                flag = true;
                System.out.println(" *** PASSWORD CHANGE CANCELLED *** ");
            }
            else {
                System.out.println(" *** ENTER A VALID CHOICE (Y/N) *** ");
            }

        } while (!flag);
    }

    // Set the mailbox capacity
    public void set_mailCap() {
        // display the current capacity
        System.out.println(this.firstName + " " + this.lastName + "'s current mail capacity: " + this.mailCapacity + " mb");

        System.out.println("WOULD YOU LIKE TO CHANGE THE MAIL CAPACITY? (Y/N) ");
        char choice = userInput.next().charAt(0);
        if (choice == 'Y' || choice == 'y') {
            System.out.println("Enter new mail capacity: ");
            this.mailCapacity = userInput.nextInt();
            // display capacity changed confirmation
            System.out.println(" *** MAILBOX CAPACITY UPDATED *** ");
            System.out.println("NEW MAILBOX CAPACITY: " + this.mailCapacity + " mb");
        }
        else if (choice == 'N' || choice == 'n') {
            System.out.println(" *** MAILBOX CAPACITY CHANGE CANCELLED *** ");
            System.out.println("CURRENT MAILBOX CAPACITY: " + this.mailCapacity + " mb");
        }
        else {
            System.out.println(" *** INVALID SELECTION ***");
        }


    }

    // Set the alternate email
    public void alternate_email() {
        System.out.print("Enter new alternate email: ");
        this.alter_email = userInput.next();
        System.out.println("ALTERNATE EMAIL SET SUCCESSFULLY!");
    }

    // Displaying the employee's information
    public void getInfo() {
        System.out.println("NAME: " + this.firstName + " " + this.lastName);
        System.out.println("DEPARTMENT: " + this.department);
        System.out.println("EMAIL: " + this.email);
        System.out.println("PASSWORD: " + this.password);
        System.out.println("MAILBOX CAPACITY: " + this.mailCapacity + "mb");
        System.out.println("ALTER EMAIL: " + this.alter_email);
    }

    public void runApp(Email email) {
        int choice;
        do {
            System.out.println("\n**** ENTER YOUR CHOICE ****\n1.Show Info\n2.Update Password\n3.Update Mailbox Capacity\n4.Enter Alternate Email\n5. Exit\n***************************");

            choice = userInput.nextInt();
            switch(choice) {
                case 1:
                    email.getInfo();
                    break;
                case 2:
                    email.set_password();
                    break;
                case 3:
                    email.set_mailCap();
                    break;
                case 4:
                    email.alternate_email();
                    break;
                case 5:
                    System.out.println("\nTHANKS!!!");
                    break;
                default:
                    System.out.println(" *** INVALID CHOICE! ENTER AGAIN! ***");
            }
        } while (choice != 5);
    }
}
