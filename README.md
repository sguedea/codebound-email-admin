# Today we will be working on finishing up CodeBound's Email Application
This Java Project is focused on applying object-oriented design in a real world application.

## INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here. 
4. Make sure you go through all the files!

## You have been assigned the task of creating email accounts for new hires.

## Your application should do the following:

* Generate an email with the following syntax: firstname.lastname@department.company.com
* Determine the department (sales, development, medical), in none leave blank.
* Generate a random String for a password (should be a length of 8 characters)
* Have set methods to change the password, set the mailbox capacity, and define an alternate email address
* Have get methods to display the name, email, and mailbox capacity.

Bonus
* Make the application keep its data in a file named recentHires.txt so that the we can keep track of the new hires.





    

